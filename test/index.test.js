/* globals mocha, chai, before, describe, it */
import jsCacheFetch, { getCacheKeyName } from "../index.js";

mocha.setup("bdd");

const RESOURCE = "https://api.ipdata.co/?api-key=test";
const CACHE_KEY_NAME = getCacheKeyName(RESOURCE);

before(function () {
  localStorage.clear();
});

describe("cacheFetch()", function () {
  it("should fetch remote resource successfully", async function () {
    const response = await jsCacheFetch(RESOURCE);
    chai.expect(response.ok).to.equal(true);
    chai.expect(response.url).to.not.equal("");
  });

  it(`should set cache in localStorage under key name: "${CACHE_KEY_NAME}"`, function () {
    const cachedResponse = localStorage.getItem(CACHE_KEY_NAME);
    chai.expect(cachedResponse).to.not.be.null;
  });

  it("should fetch second request from cache", async function () {
    const response = await jsCacheFetch(RESOURCE);
    chai.expect(response.ok).to.equal(true);
    chai.expect(response.url).to.equal("");
    chai.expect(response.type).to.equal("default");
  });
});

mocha.checkLeaks();
mocha.run();
