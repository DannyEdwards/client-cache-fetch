"use-strict";

const now = new Date();
const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

const getCache = (keyName: string) => localStorage.getItem(keyName);
const setCache = (keyName: string, keyValue: string) =>
  localStorage.setItem(keyName, keyValue);

export const getCacheKeyName = (keyNamePrefix: string) =>
  `${keyNamePrefix}-${today.getTime()}`;

export default (resource: string, init = {}): Promise<Response> => {
  const url = new URL(resource);
  const cacheKeyName = getCacheKeyName(url.href);
  const cache = getCache(cacheKeyName);

  return cache !== null
    ? new Promise((resolve) => resolve(new Response(cache)))
    : fetch(resource, init).then((response) => {
        if (response.ok) {
          response
            .clone()
            .text()
            .then((text) => setCache(cacheKeyName, text));
        }
        return response;
      });
};
