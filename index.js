"use-strict";
var now = new Date();
var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
var getCache = function (keyName) { return localStorage.getItem(keyName); };
var setCache = function (keyName, keyValue) {
    return localStorage.setItem(keyName, keyValue);
};
export var getCacheKeyName = function (keyNamePrefix) {
    return keyNamePrefix + "-" + today.getTime();
};
export default (function (resource, init) {
    if (init === void 0) { init = {}; }
    var url = new URL(resource);
    var cacheKeyName = getCacheKeyName(url.href);
    var cache = getCache(cacheKeyName);
    return cache !== null
        ? new Promise(function (resolve) { return resolve(new Response(cache)); })
        : fetch(resource, init).then(function (response) {
            if (response.ok) {
                response
                    .clone()
                    .text()
                    .then(function (text) { return setCache(cacheKeyName, text); });
            }
            return response;
        });
});
