# Client Cache Fetch

Helper for fetching and caching text responses using `localStorage`.

## Installation

```shell
npm install client-cache-fetch
```

## Syntax

Client cache fetch inherits the same API as [`fetch()`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch).

> cacheFetchResponsePromise = cacheFetch(resource, init);

## Example

```js
import cacheFetch from "client-cache-fetch";

const reponse = await cacheFetch("https://api.ipdata.co/?api-key=test", {
  method: "GET",
  headers: {
    "Content-Type": "text/json",
  },
});
```
