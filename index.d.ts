export declare const getCacheKeyName: (keyNamePrefix: string) => string;
declare const _default: (resource: string, init?: {}) => Promise<Response>;
export default _default;
